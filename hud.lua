function onTick()
    altimeter = input.getNumber(1)
    speed = input.getNumber(2)
    tilt1 = input.getNumber(3)
    tilt2 = input.getNumber(4)
end

-- Draw function that will be executed when this script renders to a screen
function onDraw()
    w = screen.getWidth() -- Get the screen's width and height
    h = screen.getHeight()

    screen.setColor(0, 255, 0)
    screen.drawText(0, 0, string.format('%.1f', altimeter))

    screen.setColor(255, 0, 0)
    screen.drawText(70, 0, string.format('%.1f', speed))
    
	d = tilt2 * 8 *48
	screen.drawLine(0, (h / 2) + (tilt1 * math.pi * 180) + d, w, (h / 2) + (tilt1 * math.pi * 180) -d)
end
