function onTick()
    gpsX = input.getNumber(1)
    gpsY = input.getNumber(2)
    altitude = input.getNumber(3)
    targetX = input.getNumber(4)
    targetY = input.getNumber(5)
    compass = input.getNumber(6)
    tilt1 = input.getNumber(7)
    tilt2 = input.getNumber(8)

    -- Compass returns -0.5 to 0.5 where negative values represent 0-180 degrees
    -- We convert it to returns 0 to 1
    if compass < 0 then
        compass = 1 + compass
    end

    dX = targetX - gpsX
    dY = targetY - gpsY
    distanceToTarget = math.sqrt(dX * dX + dY * dY)

    if distanceToTarget == 0 then
        roll = 1
    else
        roll = math.abs(math.asin(dX / distanceToTarget)) / math.pi / 2
    end

    if dX > 0 and dY < 0 then
        roll = (math.abs(math.acos(dX / distanceToTarget)) / math.pi / 2) + 0.25
    elseif dX < 0 and dY < 0 then
        roll = (math.abs(math.asin(dX / distanceToTarget)) / math.pi / 2) + 0.5
    elseif dX < 0 and dY > 0 then
        roll = (math.abs(math.asin(dY / distanceToTarget)) / math.pi / 2) + 0.75
    end

    roll = (roll + compass) % 1 -- If we get for example 1.2 we want only 0.2 so we need modulo

    absoluteDistanceToTarget = math.sqrt(altitude * altitude + distanceToTarget * distanceToTarget)
    if absoluteDistanceToTarget == 0 then
        pitch = 0
    else
        pitch = (math.acos(altitude / absoluteDistanceToTarget) / math.pi / 2) + math.abs(((tilt1 / roll) * roll))
    end

    output.setNumber(1, roll)
    output.setNumber(2, pitch / 0.7)
end

-- Draw function that will be executed when this script renders to a screen
function onDraw()
end
