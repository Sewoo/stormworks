function onTick()
    temperature = input.getNumber(1)
    fuel = input.getNumber(2)
    fuelFlow = input.getNumber(3)
    torque = input.getNumber(4)
    electrocity = input.getNumber(5)
    rps = input.getNumber(6)
end

function onDraw()
    w = screen.getWidth()
    h = screen.getHeight()

    screen.setColor(0, 255, 0)
    screen.drawRectF(0, h, 4, (temperature / 120) * h * -1)
    screen.drawRectF(6, h, 4, fuel / 2109 * h * -1)
    screen.drawRectF(10, h, 4, fuelFlow/10 * h * -1)
    screen.drawRectF(16, h, 4, torque / 40 * h * -1)
    screen.drawRectF(22, h, 4, electrocity * h * -1)
    screen.drawRectF(28, h, 4, rps / 21 * h * -1)

    screen.setColor(255, 0, 0)
    screen.drawText(0, 0, "T")
    screen.drawText(6, 0, "F")
    screen.drawText(10, 0, "L")
    screen.drawText(16, 0, "Q")
    screen.drawText(22, 0, "E")
    screen.drawText(28, 0, "R")
end
